<div align="center">
    <p>
        <img src="https://ik.imagekit.io/taskordimg/logo_8lLu9EPFa.svg" height="70" alt="Taskord Logo">
    </p>
    <h1>Taskord</h1>
    <strong>✅ Get things done in public</strong>
</div>
<br>
<div align="center">
    <a href="https://gitlab.com/taskord/taskord/-/commits/main">
        <img src="https://img.shields.io/gitlab/pipeline/taskord/taskord/main?label=build" alt="Build CI">
    </a>
    <a href="https://gitlab.com/taskord/taskord-deploy/-/pipelines">
        <img src="https://img.shields.io/gitlab/pipeline/taskord/taskord-deploy/master?color=%236a63ec&label=deploy" alt="Deploy">
    </a>
    <a href="https://github.com/taskordhq/web/actions/workflows/test.yml">
        <img src="https://github.com/taskordhq/web/actions/workflows/test.yml/badge.svg?branch=main" alt="GitHub Action">
    </a>
    <a href="https://gitlab.styleci.io/repos/20359920?branch=main">
        <img src="https://gitlab.styleci.io/repos/20359920/shield" alt="Style CI">
    </a>
    <a href="https://www.codacy.com/gl/taskord/taskord/dashboard">
        <img src="https://app.codacy.com/project/badge/Grade/7a9d7f0b31cb472185605c5089a6e2d8" alt="CodeClimate"/>
    </a>
    <a href="https://app.honeybadger.io/project/Da5eoB0yRe">
        <img src="https://img.shields.io/badge/honeybadger-active-informational" alt="Honeybadger badge">
    </a>
    <a href="https://www.php.net">
        <img src="https://img.shields.io/badge/PHP-v8.0-green.svg" alt="PHP version">
    </a>
    <a href="https://laravel.com">
        <img src="https://img.shields.io/badge/Laravel-v8.x-brightgreen.svg" alt="Laravel version">
    </a>
    <a href="LICENSE">
        <img src="https://img.shields.io/badge/license-MIT-green?longCache=true" alt="MIT License">
    </a>
    <a href="https://gitlab.com/taskord/taskord">
        <img src="https://img.shields.io/github/languages/code-size/taskordhq/web" alt="Code size">
    </a>
    <a href="https://gitlab.com/taskord/taskord/-/commits/main">
        <img src="https://badgen.net/gitlab/last-commit/taskord/taskord" alt="Last commit">
    </a>
    <a href="https://discord.gg/9M4Q65b">
        <img src="https://img.shields.io/discord/742712073670230026.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2" alt="Discord">
    </a>
    <a href="https://twitter.com/taskord">
        <img src="https://img.shields.io/twitter/follow/taskord?label=Follow&style=social" alt="Taskord Twitter">
    </a>
</div>
<div align="center">
    <br>
    <a href="https://taskord.com"><b>taskord.com »</b></a>
    <br><br>
    <a href="https://gitlab.com/taskord/taskord/-/issues/new"><b>Report Bug</b></a>
    •
    <a href="https://gitlab.com/taskord/taskord/-/issues/new"><b>Request Feature</b></a>
</div>

## About Taskord

- **✅ Tasks:** All tasks are public and added to your maker profile.
- **🔥 Reputation:** Earn reputations by completing, liking, and commenting on tasks and questions, which helps you to stay productive.
- **😀 Makers:** Community of peoples who ships constantly.
- **📦 Products:** Ship your products to Taskord and make regular updates about the product and even add tasks to them.
- **💬 Q&A:** Get your questions answered and use this feature as discussion too.
- **⛳ Milestones:** Add tasks to the milestone and accomplish them in time.
- **🤝 Meetups:** Find or create your own meetup and meet people near you who share your interests.
- **🎁 Deals:** Discounts and special deals for Taskord members. Only available to patrons.

## License

Taskord is open-sourced software licensed under the [MIT license](LICENSE).
